package ictgradschool.industry.lab04.ex07;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    String brand;
    String model;
    double price;

    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    // TODO Insert getModel() method here
    public String getModel() {
        return model;
    }

    // TODO Insert setModel() method here
    public void setModel(String model) {
        this.model = model;
    }

    // TODO Insert getPrice() method here
    public double getPrice() {
        return price;
    }

    // TODO Insert setPrice() method here
    public void setPrice(double price) {
        this.price = price;
    }

    // TODO Insert toString() method here
    public String toString() {
        String name = (brand + " " + model + ".");
        return name;
    }

    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(MobilePhone other) {
        if (this.price < price) {
            return true;
        }
        return false;
    }

    // TODO Insert equals() method here
    public boolean equals(MobilePhone other){
        if (this.brand.equals(brand)&&this.model.equals(model)){
            return true;
        }
        return false;
    }
}


