package ictgradschool.industry.lab04.ex05;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public static final int QUIT = 4;


    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.print("Hi! What is your name? ");
        String name= Keyboard.readInput();
        System.out.print("\n");

        while (true) {
            int playerChoice = promptPlayerGetChoice();
            int computerChoice = (int) (3 * Math.random() + 1);

            if (playerChoice == QUIT) {
                System.out.println("Goodbye "+name+". Thanks for playing :)");
                break;
            }
            System.out.print("\n");
            displayPlayerChoice(name, playerChoice);
            displayPlayerChoice("Computer", computerChoice);

            boolean userWon = userWins(playerChoice, computerChoice);
            String result = getResultString(playerChoice, computerChoice);

            if (userWon) {
                // Player won
                System.out.println(name + " wins because " + result +"\n");
            } else if (playerChoice == computerChoice) {
                // Tie
                System.out.println("No one wins.\n");
            } else {
                // Computer won
                System.out.println("The computer wins becuase " + result + "\n");
            }
        }
    }

    public int promptPlayerGetChoice() {
        System.out.println("1. Rock");
        System.out.println("2. Scissors");
        System.out.println("3. Paper");
        System.out.println("4. Quit");
        System.out.print("\t Enter choice: ");
        return Integer.parseInt(Keyboard.readInput());
    }

    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)

        if (choice==ROCK){
            System.out.println(name+" chose rock.");
        }else if (choice==SCISSORS){
            System.out.println(name+" chose Scissors.");
        }else if (choice==PAPER){
            System.out.println(name+" chose Paper.");
        }
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.

        if (playerChoice==ROCK){
            if (computerChoice==SCISSORS){
                return true;
            }
        }else if (playerChoice==SCISSORS){
            if (computerChoice==PAPER){
                return true;
            }
        }else if (playerChoice==PAPER){
            if (computerChoice==ROCK){
                return true;
            }
        }

        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        String result;
        if (playerChoice==computerChoice){
            result = TIE;
            return result;
        }
        else if ((playerChoice==1&&computerChoice==2)||(playerChoice==2&&computerChoice==1)){
            result = ROCK_WINS;
            return result;
        }
        else if ((playerChoice==3&&computerChoice==2)||(playerChoice==2&&computerChoice==3)) {
            result = SCISSORS_WINS;
            return result;
        }else{
            result = PAPER_WINS;
        }
        return result;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
