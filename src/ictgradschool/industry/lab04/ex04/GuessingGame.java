package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int goal=(int)(100*Math.random());
        int guess=0;

       while (guess!=goal){
            System.out.print("Enter your guess (1-100): ");
            guess= Integer.parseInt(Keyboard.readInput());
            while (guess!=goal) {
                while (guess > goal) {
                    System.out.print("Too high, try again");
                    System.out.print("Enter your guess (1-100): ");
                    guess = Integer.parseInt(Keyboard.readInput());
                }
                while (guess < goal) {
                    System.out.print("Too low, try again");
                    System.out.print("Enter your guess (1-100): ");
                    guess = Integer.parseInt(Keyboard.readInput());
                }
                if(guess==goal) {
                    System.out.println("Perfect! ");
                    System.out.print("Goodbye");
                    break;}
            }


        }



    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
