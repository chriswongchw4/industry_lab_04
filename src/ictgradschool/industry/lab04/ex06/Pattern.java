package ictgradschool.industry.lab04.ex06;

/**
 * Created by chw4 on 7/03/2017.
 */
public class Pattern {

    char symbol;
    int repetitions;

    public Pattern(int repeats, char character) {
        repetitions=repeats;
        symbol=character;
    }

    public void setNumberOfCharacters(int num) {
        repetitions = num;
    }

    public int getNumberOfCharacters() {
        return repetitions;
    }

    public String toString() {
        String line = "";

        for (int i = 0; i < repetitions; i++) {
            line += symbol;
        }
        return line;
    }
}
